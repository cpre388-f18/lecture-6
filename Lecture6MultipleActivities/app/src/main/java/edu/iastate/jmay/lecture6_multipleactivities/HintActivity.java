package edu.iastate.jmay.lecture6_multipleactivities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class HintActivity extends AppCompatActivity {
    // This is the key used to pass the hint string from MainActivity to HintActivity via an
    // explicit intent.  Notice how the package name is used to avoid being overwritten by other
    // apps that use intents.
    private static final String HINT_INTENT_KEY
            = "edu.iastate.jmay.lecture6_multipleactivities.hint";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hint);

        // Locate the explicit intent.  The action is ignored, because there's only one action we
        // will take, which is showing the hint.  The data is read from the "extras", using the
        // defined key.
        String hintText = getIntent().getStringExtra(HINT_INTENT_KEY);
        // To go with MainActivity:55, this next line works, but violates best practices.
//        String hintText = getIntent().getStringExtra("hintText");
        TextView hintTextView = findViewById(R.id.hintTextView);
        hintTextView.setText(hintText);
    }

    /**
     * Creates an explicit intent that launches HintActivity as the action, with the hint string as
     * the data.  This method is intended to be called from MainActivity, not HintActivity.
     * @param packageContext the activity (or other Context) that is launching HintActivity
     * @param hint the string of the hint for HintActivity to display
     * @return the instance of information for triggering an explicit intent
     */
    public static Intent newIntent(Context packageContext, String hint) {
        // Create the explicit intent, defining the action to be this activity.
        Intent hintIntent = new Intent(packageContext, HintActivity.class);
        // Store the "extra" data, which is the hint text.
        hintIntent.putExtra(HINT_INTENT_KEY, hint);
        // Return the new Intent (instead of calling it here) to allow MainActivity to send the
        // intent.
        return hintIntent;
    }
}

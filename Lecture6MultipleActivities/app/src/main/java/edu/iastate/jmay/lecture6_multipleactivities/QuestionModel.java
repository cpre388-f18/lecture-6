package edu.iastate.jmay.lecture6_multipleactivities;

public class QuestionModel {
    private String question;
    private boolean answer;
    private String hint;

    public String getHint() {
        return hint;
    }

    public QuestionModel(String question, boolean answer, String hint) {
        this.question = question;
        this.answer = answer;
        this.hint = hint;
    }

    public String getQuestion() {
        return question;
    }

    public boolean checkAnswer(boolean guess) {
        return guess == answer;
    }

}

package edu.iastate.jmay.lecture6_multipleactivities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView questionTextView;
    private TextView scoreTextView;
    private QuestionModel[] questions;
    private int questionIndex;
    private int score;

    private QuestionModel[] getSampleQuestions() {
        QuestionModel[] questions = new QuestionModel[3];
        questions[0] = new QuestionModel(getString(R.string.question1), true, getString(R.string.question1hint));
        questions[1] = new QuestionModel(getString(R.string.question2), false, getString(R.string.question2hint));
        questions[2] = new QuestionModel(getString(R.string.question3), false, getString(R.string.question3hint));
        return questions;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        questionTextView = findViewById(R.id.questionTextView);
        scoreTextView = findViewById(R.id.scoreTextView);
        questions = getSampleQuestions();
        questionIndex = 0;
        score = 0;
        updateLayout();
    }

    public void onButtonTrueClick(View v) {
        doGuess(true);
    }

    public void onButtonFalseClick(View v) {
        doGuess(false);
    }

    public void onButtonHintClick(View v) {
        // Construct the information to send as the explicit intent.
        Intent hintIntent = HintActivity.newIntent(this, questions[questionIndex].getHint());
        // Send the explicit intent to the Android operating system, which will instantiate the
        // HintActivity.
        startActivity(hintIntent);

        // The following code works, but violates best practices.  See Lecture 6 presentation slides
        // for the description of best practices.
//        Intent explicitIntent = new Intent(this, HintActivity.class);
//        explicitIntent.putExtra("hintText", questions[questionIndex].getHint());
//        startActivity(explicitIntent);
    }

    private void doGuess(boolean guess) {
        if (questions[questionIndex].checkAnswer(guess)) {
            // Notify the user
            Toast.makeText(this, R.string.correct, Toast.LENGTH_SHORT).show();

            // Move to the next question
            nextQuestion();

            // Update the score
            score++;
        } else {
            Toast.makeText(this, R.string.incorrect, Toast.LENGTH_SHORT).show();
        }
        // Update the layout (MVC view)
        updateLayout();
    }

    private void nextQuestion() {
        questionIndex++;
        if (questionIndex >= questions.length) {
            questionIndex = 0;
        }
    }

    private void updateLayout() {
        questionTextView.setText(questions[questionIndex].getQuestion());
        scoreTextView.setText(getString(R.string.score, score));
    }
}

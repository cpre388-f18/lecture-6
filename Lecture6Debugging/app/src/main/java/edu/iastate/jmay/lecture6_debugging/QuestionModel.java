package edu.iastate.jmay.lecture6_debugging;

public class QuestionModel {
    private String question;
    private boolean answer;

    public QuestionModel(String question, boolean answer) {
        this.question = question;
        this.answer = answer;
    }

    public String getQuestion() {
        return question;
    }

    public boolean checkAnswer(boolean guess) {
        return guess == answer;
    }

}

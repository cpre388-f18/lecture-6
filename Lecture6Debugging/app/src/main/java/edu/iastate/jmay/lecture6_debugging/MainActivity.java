package edu.iastate.jmay.lecture6_debugging;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView questionTextView;
    private TextView scoreTextView;
    private QuestionModel[] questions;
    private int questionIndex;
    private int score;

    private QuestionModel[] getSampleQuestions() {
        QuestionModel[] questions = new QuestionModel[3];
        questions[0] = new QuestionModel(getString(R.string.question1), true);
        questions[1] = new QuestionModel(getString(R.string.question2), false);
        // BUG
        // Find this in the Logcat stack trace
        // This exception happens when the app starts.  The easiest way to find it is by looking at
        // the Logcat exception trace.
        questions[3] = new QuestionModel(getString(R.string.question3), false);
        return questions;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        questionTextView = findViewById(R.id.questionTextView);
        scoreTextView = findViewById(R.id.scoreTextView);
        questions = getSampleQuestions();
        questionIndex = 0;
        score = 0;
        updateLayout();
    }

    public void onButtonTrueClick(View v) {
        doGuess(true);
    }

    public void onButtonFalseClick(View v) {
        doGuess(false);
    }

    private void doGuess(boolean guess) {
        if (questions[questionIndex].checkAnswer(guess)) {
            // Notify the user
            Toast.makeText(this, R.string.correct, Toast.LENGTH_SHORT).show();

//            score++;

            // Move to the next question
            nextQuestion();
        } else {
            Toast.makeText(this, R.string.incorrect, Toast.LENGTH_SHORT).show();
        }
        // BUG
        // The condition being checked here is done AFTER nextQuestion() modified questionIndex.
        // This bug is intended to be found by setting a breakpoint on line 49 and using step over
        // while watching questionIndex to realize how it gets updated.  To fix, move score++ to
        // line 53, which is the same condition, but done in order.
        if (questions[questionIndex].checkAnswer(guess)) {
            score++;
        }
        // Update the layout (MVC view)
        updateLayout();
    }

    private void nextQuestion() {
        questionIndex++;
        // BUG
        // Find this by breaking on uncaught exceptions
        // The mistake below is a > comparison instead of a >= comparison (because questionIndex is
        // indexed from 0).  This causes an out of bounds error elsewhere.
        if (questionIndex > questions.length) {
            questionIndex = 0;
        }
    }

    private void updateLayout() {
        // This may cause an uncaught exception (depending on the order of bug fixing).  Remember to
        // break on uncaught exceptions in Run -> View Breakpoints...
        // This bug should be fixed in nextQuestion(), not here.
        questionTextView.setText(questions[questionIndex].getQuestion());
        scoreTextView.setText(getString(R.string.score, score));
    }
}
